import * as React from "react"
import logo from '../../images/logo.svg'
import logo2 from '../../images/logo2.png'
import * as styles from "../../css/style.module.css"
import { CgFormatSlash } from "react-icons/cg";
import { RiMenu2Line } from "react-icons/ri";
import { AiOutlineClose } from "react-icons/ai";
import useMediaQuery from '@material-ui/core/useMediaQuery';
const Header = (props) => {

    function servicesClick() {
        if (!props.isServices && !props.menuServices) {
            var menu = document.getElementById("menuService");
            menu.classList.toggle(styles.hidden);

            var slashRight = document.getElementById("slashRight");
            slashRight.classList.toggle(styles.hidden);

            var slashLeft = document.getElementById("slashLeft");
            slashLeft.classList.toggle(styles.hidden);
        }
    }

    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }

    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <main className={props.homepage ? styles.absolute : ""}>
            <div className={styles.top}>
                <div className={styles.menuService}>
                    <p className={`${props.joinUs ? styles.blanc : ""} ${styles.headerText}`}>
                        <span className={styles.show} id="slashRight">
                            <CgFormatSlash
                                style={{ margin: '-10px' }}
                                size="40px"
                            />
                        </span>
                        <span className={styles.services} aria-hidden="true" onClick={servicesClick} onKeyDown={servicesClick}> Services </span>
                        <span className={styles.hidden} id="slashLeft">
                            <CgFormatSlash
                                style={{ margin: '-10px' }}
                                size="40px"
                            />
                        </span>
                    </p>

                    <ul className={styles.hidden} id="menuService">
                        <li><a href="/services/creation-contenu/" className={`${props.joinUs ? styles.blanc : ""} ${styles.lien}`}>CRÉATION DE CONTENU</a></li>
                        <li><a href="services/audit-instagram/" className={`${props.joinUs ? styles.blanc : ""} ${styles.lien}`}>AUDIT INSTAGRAM</a></li>
                        <li><a href="services/strategie-social-media/" className={`${props.joinUs ? styles.blanc : ""} ${styles.lien}`}>STRATÉGIE SOCIAL MEDIA</a></li>
                        <li><a href="services/coaching-instagram/" className={`${props.joinUs ? styles.blanc : ""} ${styles.lien}`}>COACHING INSTAGRAM</a></li>
                    </ul>
                </div>
                {props.joinUs
                    ? <a href="/"><img src={logo2} className={styles.logo} alt="logo" /></a>
                    : <a href="/"><img src={logo} className={styles.logo} alt="logo" /></a>}

                <RiMenu2Line
                    size="40px"
                    onClick={openNav}
                    style={{
                        cursor: "pointer",
                    }}
                    className={styles.responsive}
                />

                <div id="myNav" className={styles.overlay}>
                    <div>
                        <img src={logo} className={styles.logo} alt="logo" />
                        <AiOutlineClose
                            onClick={closeNav}
                            color="black"
                            size="40px"
                            style={{
                                cursor: "pointer",
                            }}
                        />
                    </div>

                    <div className={styles.overlayContent}>
                        <a href="/">
                            <CgFormatSlash
                                style={{ margin: '-10px' }}
                                size="40px"
                            />
                            SERVICES
                        </a>
                        <ul>
                            <li><a href="../../services/creation-contenu/">CRÉATION DE CONTENU</a></li>
                            <li><a href="../../services/audit-instagram/">AUDIT INSTAGRAM</a></li>
                            <li><a href="../../services/strategie-social-media/">STRATÉGIE SOCIAL MEDIA</a></li>
                            <li><a href="../../services/coaching-instagram/">COACHING INSTAGRAM</a></li>
                        </ul>

                        <a href="/aPropos/aPropos/">
                            <CgFormatSlash
                                style={{ margin: '-10px' }}
                                size="40px"
                            />
                            A PROPOS
                        </a>
                        <a href="/contact">
                            <CgFormatSlash
                                style={{ margin: '-10px' }}
                                size="40px"
                            />
                            CONTACT
                        </a>
                    </div>
                </div>

                <a href="/contact">
                    <p className={`${props.joinUs ? styles.blanc : ""} ${styles.headerText}`}>
                        <CgFormatSlash
                            style={{ margin: '-10px' }}
                            size="40px"
                        />
                        Contact
                    </p>
                </a>
            </div>
            <a href="/aPropos/aPropos/"><p className={`${styles.aPropos} ${styles.headerText}`}>/ A PROPOS</p></a>
            {props.isServices && !mq ?
                <ul className={styles.menuServiceInline} id="">
                    <li><a href="../../services/creation-contenu/" className={styles.lien}>CRÉATION DE CONTENU</a></li>
                    <li><a href="../../services/audit-instagram/" className={styles.lien}>AUDIT INSTAGRAM</a></li>
                    <li><a href="../../services/strategie-social-media/" className={styles.lien}>STRATÉGIE SOCIAL MEDIA</a></li>
                    <li><a href="../../services/coaching-instagram/" className={styles.lien}>COACHING INSTAGRAM</a></li>
                </ul>
                : ""}
        </main>
    )
}

export default Header