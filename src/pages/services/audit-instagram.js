import * as React from "react"
import Header from "../header/Header"
import Footer from "../../templates/Footer";

import * as styles from '../../css/style.module.css'
import * as servicesStyle from '../../css/services.module.css'

import image2 from "../../images/audit-instagram/1.1.png"
import image1 from "../../images/audit-instagram/2.1.png"
import fleche from '../../images/fleche.svg'
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function AuditInstagram() {
    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <main>
            <div className={servicesStyle.divServiceResponsive}>
                <Header isServices={true} />
                <h2>AUDIT INSTAGRAM</h2>
                <p className={styles.pServices}>Que tu communiques sur Instagram pour ta marque depuis plusieurs années ou quelques mois, il est important de
                    repositionner en permanence ta stratégie ainsi que tes objectifs. La meilleure manière pour cela est de mener un audit
                    de ton compte Instagram. Il s’agit de s’assurer que ta présence est alignée avec tes objectifs marketing, et que tous
                    les efforts déployés vont t’aider à vendre plus.</p>

                {mq ? <img src={fleche} alt="fleche" style={{ width: '30px' }} className={styles.fleche} /> : ""}
            </div>
            <div className={servicesStyle.divServiceResponsive2}>
                <div className={servicesStyle.section1}>
                    <div className={servicesStyle.listSection1}>
                        <h6>Ce que je te propose pendant l’audit Instagram :</h6>
                        <p><span>01</span>Analyser les forces et les faiblesses de ton compte Instagram</p>
                        <p><span>02</span>Donner des recommandations pour ton compte Instagram </p>
                        <p><span>03</span>Un livrable du dossier sous PDF (48h – 72h)</p>
                        <p><span>04</span>Le prix de la prestation varie en fonction du nombre de recommendations</p>
                    </div>
                    <div className={servicesStyle.img}>
                        <img src={image1} alt="" />
                    </div>
                </div>

                <div className={servicesStyle.section2}>
                    <div className={servicesStyle.img}>
                        <img src={image2} alt="" />
                    </div>
                    <div className={servicesStyle.listSection2}>
                        <h6>Les avantages de faire un audit Instagram :</h6>
                        <p>Un échange par téléphone pour valider les sujets à aborder dans les publications </p>
                        <p>Création de visuels (publications + stories) </p>
                        <p>Ecriture des légendes dans les publications </p>
                        <p>Sélection des hashtags pertinents ( 15 – 30 ). </p>
                        <p>Livraison des posts en partage sur Google Drive. </p>
                        <p>Les tarifs sont définis en fonction du nombre de posts.</p>
                    </div>
                </div>

                <Footer isServices={true} />
            </div>
        </main>
    );
}