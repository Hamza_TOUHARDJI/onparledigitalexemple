import * as React from "react"
import Header from "../header/Header"
import Footer from "../../templates/Footer";

import * as styles from '../../css/style.module.css'
import * as servicesStyle from '../../css/services.module.css'

import image1 from "../../images/strategie-social-media/1.1.png"
import image2 from "../../images/strategie-social-media/2.1.png"
import fleche from '../../images/fleche.svg'
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function StrategieSocialMedia() {
    const mq = useMediaQuery("(max-width: 700px)");

    return (
        <main>

            <div className={servicesStyle.divServiceResponsive}>
                <Header isServices={true} />
                <h2>STRATÉGIE SOCIAL MEDIA</h2>
                <p className={styles.pServices}>La stratégie d’utilisation des réseaux sociaux est la synthèse de tout ce que tu envisages de faire et espères accomplir
                    sur les médias sociaux. Elle guide tes actions et t’indiques tes réussites et tes échecs. Une communication performante
                    sur les réseaux sociaux va permettre de générer des leads, d’augmenter l’acquisition de nouveaux clients, d’accroître ta
                    visibilité ou encore d’attirer de nouveaux talents.</p>
                {!mq ? <img src={fleche} alt="fleche" style={{ width: '30px' }} className={styles.fleche} /> : ""}
            </div >
            <div className={servicesStyle.divServiceResponsive2}>
                <div className={servicesStyle.section1}>
                    <div className={servicesStyle.listSection1}>
                        <h6>Cet accompagnement va te permettre :</h6>
                        <p>Améliorer ton image de marque en créant une identité forte, pour donner confiance aux clients. </p>
                        <p>Accroître ta notoriété en construisant une image propre à laquelle ton audience peut se greffer et se reconnaître. </p>
                        <p>Augmenter ton engagement en proposant un contenu utile, de valeur, incitant ainsi ta communauté à s’engager. </p>
                        <p>Cibler les besoins de ton audience grâce à des questionnaires et des sondages. </p>
                        <p>Fidéliser et trouver des nouveaux clients grâce à une communication authentique te permettant d’avoir une relation proche et durable. </p>
                        <p>Générer plus de trafic en créant du contenu intéressant sur les réseaux sociaux. </p>
                        <p>Améliorer ton référencement en partageant ton contenu sur les réseaux sociaux.</p>
                    </div>
                    <div className={servicesStyle.img}>
                        <img src={image1} alt="" />
                    </div>
                </div>

                <div className={servicesStyle.section2}>
                    <div className={servicesStyle.img}>
                        <img src={image2} alt="" />
                    </div>
                    <div className={servicesStyle.listSection2}>
                        <h6>Ce que je te propose pour la création de contenu :</h6>
                        <p>Durée de l’accompagnement sur 1 mois </p>
                        <p>3 séances en visio – conférence </p>
                        <p>Un livrable en fichier PDF </p>
                        <p>Un suivi de 1 mois par téléphone ou mail après notre coaching </p>
                        <p>Bonus : Fichier sur les tendances Instagram en PDF </p>
                        <p>Bonus : Mes outils d’organisations + Mes applications</p>
                    </div>
                </div>

                <div className={servicesStyle.formules}>
                    <div className={servicesStyle.formule1}>
                        <p>FORMULE</p>
                        <p>BASIQUE</p>
                        <p>Stratégie uniquement sur Instagram </p>
                        <p> Livrable sous PDF dans un délai de 1 mois </p>
                        <p>2 séances mensuelles en visio – conférence ( Début – Fin )</p>
                        <p>Un suivi illimité par téléphone ou mail entre les séances </p>
                        <p>Bonus : 15 idées de contenus </p>
                        <p>Cette prestation n’inclut pas le calendrier éditorial</p>
                    </div>
                    <div className={servicesStyle.formule2}>
                        <p>FORMULE </p>
                        <p>INTERMÉDIAIRE</p>
                        <p>Stratégie sur 2 réseaux sociaux de votre choix </p>
                        <p>Livrable sous PDF dans un délai de 2 mois </p>
                        <p>2 séances mensuelles de visio – conférence </p>
                        <p>Un suivi illimité par téléphone ou mail entre les séances </p>
                        <p>Bonus : Création de 4 posts pour votre compte Instagram </p>
                        <p>Bonus : Calendrier éditorial 1 mois </p>
                        <p>PDF : les applications et les outils que j’utilise</p>
                    </div>
                    <div className={servicesStyle.formule3}>
                        <p>FORMULE </p>
                        <p>INTERMÉDIAIRE</p>
                        <p>Stratégie sur 2 réseaux sociaux de votre choix </p>
                        <p>Livrable sous PDF dans un délai de 2 mois </p>
                        <p>2 séances mensuelles de visio – conférence </p>
                        <p>Un suivi illimité par téléphone ou mail entre les séances </p>
                        <p>Bonus : Création de 4 posts pour votre compte Instagram </p>
                        <p>Bonus : Calendrier éditorial 1 mois </p>
                        <p>PDF : les applications et les outils que j’utilise</p>
                    </div>
                </div>

                <Footer isServices={true} />
            </div>

        </main>
    )
}

