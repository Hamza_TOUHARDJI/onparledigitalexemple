import * as React from "react"
import Header from "../header/Header"
import Footer from "../../templates/Footer";

import * as styles from '../../css/style.module.css'
import * as servicesStyle from '../../css/services.module.css'

import image1 from "../../images/coaching-instagram/1.1.png"
import image2 from "../../images/coaching-instagram/2.1.png"
import image3 from "../../images/coaching-instagram/3.1.png"
import fleche from '../../images/fleche.svg'
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function CoachingInstagram() {
    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <main>
            <div className={servicesStyle.divServiceResponsive}>
                <Header isServices={true} />
                <h2>COACHING INSTAGRAM</h2>
                <p className={styles.pServices}>Décider de faire appel à un coach Instagram, c’est mettre toutes les chances de ton côté pour prendre le temps
                    d’optimiser ton compte, voir ce qui va et ne vas pas, notamment grâce à un audit. Un coaching va t’aider à mobiliser
                    toutes les ressources nécessaires, à avoir un oeil extérieur sur ce que tu fais et obtenir des conseils avisés. C’est
                    ainsi que ton activité pourra décoller sur Instagram.</p>

                {mq ? <img src={fleche} alt="fleche" style={{ width: '30px' }} className={styles.fleche} /> : ""}
            </div>
            <div className={servicesStyle.divServiceResponsive2}>
                <div className={servicesStyle.section1}>
                    <div className={servicesStyle.listSection1}>
                        <h6>L’avantage pour toi de prendre ce coaching c’est :</h6>
                        <p><span>01</span>D’augmenter ta productivité et atteindre efficacement tes objectifs. </p>
                        <p><span>02</span>D’apprendre plus rapidement, en pratiquant par toi même. </p>
                        <p><span>03</span>Ne plus être seul(e)s face à tes problèmes et tes interrogations. </p>
                        <p><span>04</span>Progresser à ton rythme en toute sérénité </p>
                        <p><span>05</span>Rester un acteur majeur dans la gestion de ta communication.</p>
                    </div>
                    <div className={servicesStyle.img}>
                        <img src={image1} alt="" />
                    </div>
                </div>

                <div className={servicesStyle.section2}>
                    <div className={servicesStyle.img}>
                        <img src={image2} alt="" />
                    </div>

                    <div className={servicesStyle.listSection2}>
                        <h6>Ce que je te propose pendant le coaching c’est :</h6>
                        <p><span>01</span>D’augmenter ta productivité et atteindre efficacement tes objectifs. </p>
                        <p><span>02</span>D’apprendre plus rapidement, en pratiquant par toi même. </p>
                        <p><span>03</span>Ne plus être seul(e)s face à tes problèmes et tes interrogations. </p>
                        <p><span>04</span>Progresser à ton rythme en toute sérénité </p>
                        <p><span>05</span>Rester un acteur majeur dans la gestion de ta communication.</p>
                    </div>
                </div>

                <div className={servicesStyle.section1}>
                    <div className={servicesStyle.listSection1}>
                        <h6>Ce que je te propose pour la création de contenu :</h6>
                        <p>Durée de l’accompagnement sur 1 mois </p>
                        <p>3 séances en visio – conférence </p>
                        <p>Un livrable en fichier PDF </p>
                        <p>Un suivi de 1 mois par téléphone ou mail après notre coaching </p>
                        <p>Bonus : Fichier sur les tendances Instagram en PDF </p>
                        <p>Bonus : Mes outils d’organisations + Mes applications</p>
                    </div>
                    <div className={servicesStyle.img}>
                        <img src={image3} alt="" />
                    </div>
                </div>

                <Footer isServices={true} />
            </div>
        </main>
    )
}