import * as React from "react"
import Header from "../header/Header"
import Footer from "../../templates/Footer";
import fleche from '../../images/fleche.svg'
import * as styles from '../../css/style.module.css'
import * as servicesStyle from '../../css/services.module.css'
import image2 from "../../images/creation-contenu/1.1.png"
import image1 from "../../images/creation-contenu/2.1.png"
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function CreationContenu() {
    const mq = useMediaQuery('(min-width:700px)');

    return (
        <main>

            <div className={servicesStyle.divServiceResponsive}>
                <Header isServices={true} />
                <h2>CRÉATION DE CONTENU</h2>
                <p className={styles.pServices}>
                    La première étape d’une stratégie d’Inbound Marketing réussie consiste à la création de contenu et doit permettre
                    d’attirer des prospects sur ton site internet. Le contenu est une étape indispensable pour attirer puis convertir tes
                    prospects. Il va te permettre de guider tes visiteurs en leur fournissant du contenu inédit, et surtout pertinent par
                    rapport à leur problématique.</p>
                {!mq ? <img src={fleche} alt="fleche" style={{ width: '30px' }} className={styles.fleche} /> : ""}
            </div>

            <div className={servicesStyle.divServiceResponsive2}>
                <div className={servicesStyle.section1}>
                    <div className={servicesStyle.listSection1}>
                        <h6>Déléguer la création de contenu pour ton compte Instagram va te permettre de :</h6>
                        <p><span>01</span>D’augmenter ta productivité et atteindre efficacement tes objectifs.</p>
                        <p><span>02</span>Gagner en productivité en te libérant de certaines tâches.</p>
                        <p><span>03</span>Laisser la création de contenu entre les mains d’une experte.</p>
                        <p><span>04</span>Garder la main sur la gestion de ton compte Instagram.</p>
                        <p><span>04</span>Avoir une présence régulière sur ton compte Instagram.</p>
                    </div>
                    <div className={servicesStyle.img}>
                        <img src={image1} alt="" />
                    </div>

                </div>

                <div className={servicesStyle.section2}>
                    <div className={servicesStyle.img}>
                        <img src={image2} alt="" />
                    </div>
                    <div className={servicesStyle.listSection2}>
                        <h6>Ce que je te propose pour la création de contenu :</h6>
                        <p>Un échange par téléphone pour valider les sujets à aborder dans les publications </p>
                        <p>Création de visuels (publications + stories) </p>
                        <p>Ecriture des légendes dans les publications </p>
                        <p>Sélection des hashtags pertinents ( 15 – 30 ). </p>
                        <p>Livraison des posts en partage sur Google Drive. </p>
                        <p>Les tarifs sont définis en fonction du nombre de posts.</p>
                    </div>
                </div>

                <Footer isServices={true} />
            </div>
        </main>
    )
}