import React, { useState, useEffect } from "react";
import * as styles from "../css/style.module.css"
import Panel1 from "../templates/animations/Panel1";
import Panel2 from "../templates/animations/Panel2";
import Panel3 from "../templates/animations/Panel3";
import Panel4 from "../templates/animations/Panel4";
import Header from './header/Header'
import { gsap } from "gsap"
import { ScrollTrigger } from "gsap/ScrollTrigger"
import { ScrollToPlugin } from "gsap/ScrollToPlugin"
import Image1 from '../templates/animations/image1'
import Image2 from '../templates/animations/image2'
import Icon from '../templates/Icon'
import $ from 'jquery' // important: case sensitive.
import useMediaQuery from '@material-ui/core/useMediaQuery';
import myOwnMediaQuery from "../utils/hooks/useMediaQuery";
gsap.registerPlugin(ScrollTrigger, ScrollToPlugin)
const IndexPage = () => {

  let [count, setCount] = useState("0.00");
  let mq = useMediaQuery("(max-width: 700px)");
  const isPhone = myOwnMediaQuery('(max-width: 960px)');
  useEffect(() => {
    if (window.innerWidth > 700) {
      gsap.registerPlugin(ScrollTrigger);
      let sections = gsap.utils.toArray("#panel");
      gsap.to(sections, {
        xPercent: -100 * (sections.length - 1),
        ease: "none",
        scrollTrigger: {
          trigger: "#container",
          pin: true,
          scrub: 1,
          snap: 1 / (sections.length - 1),
          // base vertical scrolling on how wide the container is so it feels more natural.
          end: "+=3500",
          onToggle: self => console.log("toggled, isActive:", self.isActive),
          onUpdate: self => { },
          onSnapComplete: ({ progress, direction, isActive }) => { setCount(progress.toFixed(2)); console.log(progress, direction, isActive); }
        }
      });
    } else {
      // Detect request animation frame
      var scroll = window.requestAnimationFrame ||
        // IE Fallback
        function (callback) { window.setTimeout(callback, 1000 / 60) };
      var elementsToShow = document.querySelectorAll('#menuAnimation');
      // Call the loop for the first time
      loop();
    }

    function loop() {
      elementsToShow.forEach(function (element) {
        if (isElementInViewport(element)) {
          document.getElementById("menuServicePanel").classList.remove(styles.hidden);
          document.getElementById("menuServicePanel").classList.add(styles.menuAniamtion);
        } else {
          document.getElementById("menuServicePanel").classList.add(styles.hidden);
          document.getElementById("menuServicePanel").classList.remove(styles.menuAniamtion);
        }
      });
      scroll(loop);
    }

    // Helper function from: http://stackoverflow.com/a/7557433/274826
    function isElementInViewport(el) {
      // special bonus for those using jQuery
      if (typeof $ === "function" && el instanceof $) {
        el = el[0];
      }
      var rect = el.getBoundingClientRect();
      return (
        (rect.top <= 0
          && rect.bottom >= 0)
        ||
        (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.top <= (window.innerHeight || document.documentElement.clientHeight))
        ||
        (rect.top >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
      );
    }

  }, [])

  return (
    <main >

      <div className={styles.container} id="container">
        {!mq ? <Header pos={count} homepage={true} /> : ""}
        <div className={styles.panel} id="panel">
          {mq ? <Header /> : ""}
          <Panel1 indice={count} />
          {mq ? <Icon /> : ""}
        </div>
        {mq ?
          <div className={styles.panel} id="panel">
            <Image2 />
            <Image1 />
          </div>
          : ""}
        <div className={styles.panel} id="panel">
          <Panel2 />
        </div>
        <div className={styles.panel} id="panel">
          <div id="menuAnimation">
            {!mq ? <Panel3 indice={count} /> : <Panel3 />}
          </div>
        </div>
        <div className={`${styles.joinUs} ${styles.panel}`} id="panel">
          <Panel4 />
        </div>
      </div>
      {!mq ? <Icon isHomePage={true} /> : ""}
    </main>
  )
}

export default IndexPage
