import React from 'react';
import { FullPage, Slide } from 'react-full-page';
import Header from '../header/Header';
import * as styles from "../../css/style.module.css"
import Panel4 from '../../templates/animations/Panel4';
import fleche from '../../images/fleche.svg'
import auteur from '../../images/auteur/1.1.png'
import Icon from '../../templates/Icon';
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function APropos() {
    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <FullPage className={styles.fullpage}>
            <Slide className={styles.slide}>
                <Header />
                <div className={styles.about1}>
                    <h1>À propos</h1>
                    <p>Je m’appelle Princia NGONDJO, je suis une grande passionnée de
                        digitale, j’ai d’abord commencé mon expérience en animant les pages
                        et les profils de mes propres réseaux sociaux.
                    </p>
                    <img alt='fleche' src={fleche} className={styles.fleche} />
                    <Icon isNotDisplay={true} />
                </div>
            </Slide>
            <Slide className={styles.slide} >
                {!mq ? <Header joinUs={true} /> : ""}
                <div className={styles.about2}>
                    <img src={auteur} alt='autor' />
                    {!mq ?
                        <div>
                            <p>Ces expériences m’ont permises de faire appel à ma créativité, ma capacité de réflexion ainsi que ma capacité d’analyse.</p>
                            <p>Dans le désir de vouloir en apprendre davantage, je me suis dirigée tout naturellement dans une formation de community management pendant 1 an.</p>
                            <p>Après l’obtention de mon diplôme, j’ai décidé de mettre mes compétences à profit pour aider les entreprises à prospérer dans un monde où la communication est en pleine mutation.</p>
                            <p>À travers mon travail, je souhaite vous transmettre tous les outils nécessaires à la bonne gestion de votre marque sur la toile.</p>
                        </div> : ""}
                </div>
                {!mq ?
                    <div>
                        <Icon />
                    </div> : ""}
            </Slide>
            {mq ?
                <Slide className={styles.slide}>
                    <p>Ces expériences m’ont permises de faire appel à ma créativité, ma capacité de réflexion ainsi que ma capacité d’analyse.</p>
                    <p>Dans le désir de vouloir en apprendre davantage, je me suis dirigée tout naturellement dans une formation de community management pendant 1 an.</p>
                    <p>Après l’obtention de mon diplôme, j’ai décidé de mettre mes compétences à profit pour aider les entreprises à prospérer dans un monde où la communication est en pleine mutation.</p>
                    <p>À travers mon travail, je souhaite vous transmettre tous les outils nécessaires à la bonne gestion de votre marque sur la toile.</p>
                </Slide>
                : ""}
            <Slide className={styles.joinUs} >
                {!mq ? <Header joinUs={true} /> : ""}
                <Panel4 />
            </Slide>
        </FullPage>
    );
}
