import * as React from "react"
import Header from "./header/Header";
import * as styles from '../css/style.module.css'
import Icon from "../templates/Icon"
class Contact extends React.Component {
    render() {
        return (
            <main>
                <Header isServices={false} />
                <div className={styles.contact}>
                    <h5>Contact</h5>
                    <h4>communique avec moi</h4>
                    <form action="/" className={styles.formContact}>
                        <div className={styles.choises}>
                            <select name="">
                                <option selected disabled>Choisir</option>
                                <option value="">Choix 1</option>
                                <option value="">Choix 2</option>
                                <option value="">Choix 3</option>
                            </select>
                        </div>


                        <div className={styles.inputForm}>
                            <input type="text" placeholder="Nom" />
                            <input type="text" placeholder="Prénom" />
                        </div>

                        <div className={styles.inputForm}>
                            <input type="text" placeholder="Téléphone" />
                            <input type="text" placeholder="E-mail" />
                        </div>

                        <textarea className={styles.textarea} rows="5" cols="50" placeholder="Rédiger votre message ici."></textarea>

                        <button className={styles.contactButton}>ENVOYER</button>
                    </form>
                    <Icon isFooter={true} />
                    <footer>© Copyright - onparledigital - tous droits réservés</footer>
                </div>

            </main>
        );
    }
}
export default Contact;