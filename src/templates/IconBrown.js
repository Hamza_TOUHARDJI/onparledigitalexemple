import React from 'react';
import * as styles from "../css/style.module.css"

import { FaInstagram, FaLinkedinIn, FaPinterest } from "react-icons/fa";


export default function Icon(props) {

    return (
        <div id='icon' className={`${styles.icon} ${props.isfixed ? styles.iconResponsiveFixed : styles.iconResponsive} ${props.isFooter ? styles.footerIcon : ""}`}>
            <FaInstagram size="30px" color="white" />
            <FaLinkedinIn size="30px" color="white" />
            <FaPinterest size="30px" color="white" />
        </div>
    );
}
