import * as React from "react"
import * as styles from "../../css/style.module.css"

export default function Panel1(props) {
    return (
        <main>
            <h1>
                <div id="right" className={`${props.indice === "0.00" ? styles.right : styles.hidden}`}>On parle</div>
                <div id="left" className={`${props.indice === "0.00" ? styles.left : styles.hidden}`}>digital</div>
            </h1>
            <div className={`${props.indice === "0.00" ? styles.subtitle : styles.hidden}`}>
                <p>Spécialisée dans </p>
                <h3>CRÉATION DE CONTENU, STRATÉGIE SOCIAL
                    MEDIA, AUDIT ET COACHING INSTAGRAM</h3>
            </div>
            <a href="/aPropos/aPropos/"><p className={`${styles.aPropos} ${styles.headerText}`}>/ A PROPOS</p></a>
            {/*!mq ?
                <div>
                    <Image2 />
                    <Image1 />
                </div>
            : ""*/}

        </main>
    );
}
