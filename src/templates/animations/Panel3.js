import * as React from "react"
import * as styles from "../../css/style.module.css"

export default function Panel3(props) {
    return (
        <main>
            <div id="menuServicePanel" className={`${props.indice === "0.67" ? styles.menuAniamtion : styles.hidden} ${styles.menu}`}>
                <div id="a"></div>
                <p><a href="/services/creation-contenu/" className={styles.liensTitel}> CRÉATION DE CONTENU </a> </p>
                <p><a href="services/audit-instagram/" className={styles.liensTitel}> AUDIT INSTAGRAM </a> </p>
                <p><a href="services/strategie-social-media/" className={styles.liensTitel}> STRATÉGIE SOCIAL MEDIA </a> </p>
                <p><a href="services/coaching-instagram/" className={styles.liensTitel}> COACHING  INSTAGRAM </a> </p>
            </div>

        </main>);

}
