import React from 'react';
import * as styles from "../../css/style.module.css"

export default function Content5() {
    return (
        <main className={styles.joinUs}>
            <div className={styles.joinUsTitle}>
                <p>Envie de me </p>
                <p className={styles.second}>rejoindre ?</p>
            </div>
            <p className={styles.joinUsText} >Inscrit toi à ma newsletter et reçois dans ta boîte mail des conseils stratégiques sur l'utilisation de tes réseaux sociaux</p>
            <form className={styles.joinUsForm}>
                <input type="text" placeholder="Prénom" />
                <input type="text" placeholder="E-mail" />
                <button className={styles.joinUsBtn}> Envoyer</button>
            </form>

        </main>
    );
}
