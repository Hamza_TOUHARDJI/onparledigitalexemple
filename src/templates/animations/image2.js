import * as React from "react"
import * as styles from "../../css/style.module.css"
import image2 from "../../images/image-scroll2/1.1.png"
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function Image2(props) {
    const mq = useMediaQuery("(max-width: 700px)");
    return <img className={`${mq ? "" : styles.imageScroll2}`} src={image2} alt="" />
}