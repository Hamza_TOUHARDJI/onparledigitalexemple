import * as React from "react"
import * as styles from "../../css/style.module.css"
import Icon from "../Icon"
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function Panel2() {
    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <main>
            <p className={styles.jeVousAide}>Je vous aide a</p>
            {!mq ? <Icon isHomePage={true} /> : ""}
        </main>
    );
}