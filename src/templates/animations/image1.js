import * as React from "react"
import image1 from "../../images/image-scroll1/1.1.png"

export default function Image1(props) {
    return <img src={image1} alt="" id="image1showOnScroll" />
}