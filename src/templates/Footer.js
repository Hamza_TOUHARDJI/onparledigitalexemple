import React from 'react';
import * as styles from '../css/style.module.css'
import Icon from './Icon'
export default function Footer(props) {
    return (
        <main>
            <div className={styles.footer}>
                {props.isServices ? <h4>À PARTIR DE 190€</h4> : ""}
                <p>Tu veux en savoir plus sur le projet, je t’invite à me contacter</p>
                <button>CONTACTE - MOI</button>
                <Icon isFooter={true} isAbsolute={false} />
                <footer>© Copyright - onparledigital - tous droits réservés</footer>
            </div>
        </main>
    );
}
