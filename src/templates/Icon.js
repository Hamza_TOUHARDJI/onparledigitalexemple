import React from 'react';
import * as styles from "../css/style.module.css"

import { FaInstagram, FaLinkedinIn, FaPinterest } from "react-icons/fa";
import useMediaQuery from '@material-ui/core/useMediaQuery';


export default function Icon(props) {
    const mq = useMediaQuery("(max-width: 700px)");
    return (
        <div id='icon' className={`
                                ${styles.icon} 
                                ${props.isNotDisplay ? styles.hidden : ""}
                                ${props.isFooter ? styles.footerIcon : ""} 
                                ${props.isHomePage ? styles.fixed : ""}
                                ${mq && !props.isFooter ? styles.iconResponsive : ""}`}>
            <FaInstagram size="30px" />
            <FaLinkedinIn size="30px" />
            <FaPinterest size="30px" />
        </div>
    );
}


